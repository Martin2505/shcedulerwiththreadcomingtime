import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        MyThread myThread = new MyThread("Employ", 33, 0, 32);
        MyThread myThread1 = new MyThread("Company", 20, 0, 10);
        MyThread myThread2 = new MyThread("People", 51, 0, 26);
        List<MyThread> list = new ArrayList<>();
        list.add(myThread);
        list.add(myThread1);
        list.add(myThread2);
        double a = 0.0;
        int check=0;
        for (int i = 0; i < list.size(); i++) {
            if (check==list.size()){
                break;
            }
            if (list.get(i).getThreadComing() >= 5) {
                list.get(i).setThreadComing(list.get(i).getThreadComing() - 5);
                a += 5;
                if (i == list.size() - 1) {
                    i = -1;
                }
                check=0;
                continue;
            } else if (list.get(i).getThreadComing() != 0) {
                a += list.get(i).getThreadComing();
                list.get(i).setThreadComing(0);
                if (i == list.size() - 1) {
                    i = -1;
                }
                check=0;
                continue;
            }
            int c = list.get(i).getTimeexecution();
            if (list.get(i).getTimeexecution() == list.get(i).getTotaltime()) {
                if (i == list.size() - 1) {
                    i = -1;
                }check++;
                continue;
            } else if (list.get(i).getTimeexecution() + 5 > list.get(i).getTotaltime()) {
                list.get(i).setTimeexecution(list.get(i).getTotaltime());
                int b = list.get(i).getTotaltime() - c;
                a += b;
                check=0;
            } else {
                check=0;
                c += 5;
                list.get(i).setTimeexecution(c);
                a += 5;
            }
            if (i == list.size() - 1) {
                i = -1;
            }
        }
        double AverageTime = (a / list.size());
        System.out.println("All TIME = " + a);
        System.out.println("Average Time = " + AverageTime);
    }
}

