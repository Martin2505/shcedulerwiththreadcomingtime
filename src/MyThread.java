public class MyThread {
    private String c;
    private int totaltime;
    private int timeexecution;
    private int threadComing;

    public MyThread(String c, int totaltime, int timeexecution, int threadComing) {
        this.c = c;
        this.totaltime = totaltime;
        this.timeexecution = timeexecution;
        this.threadComing = threadComing;
    }

    public int getThreadComing() {
        return threadComing;
    }

    public void setThreadComing(int threadComing) {
        this.threadComing = threadComing;
    }

    public String getC() {
        return c;
    }

    public void setC(String c) {
        this.c = c;
    }

    public int getTotaltime() {
        return totaltime;
    }

    public void setTotaltime(int totaltime) {
        this.totaltime = totaltime;
    }

    public int getTimeexecution() {
        return timeexecution;
    }

    public void setTimeexecution(int timeexecution) {
        this.timeexecution = timeexecution;
    }
}
